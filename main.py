#!/usr/bin/env python3
import super_powers
from super_villains import grabhorkon

super_powers.fly()
super_powers.super_speed()

grabhorkon.monolog()
grabhorkon.dying_words()
